<?php

use PHPUnit\Framework\TestCase;

class MainTest extends TestCase
{
    public function testAdd()
    {
        $a = 8;
        $b = 7;

        $main = new Base\Main;

        $this->assertEquals(15, $main->add($a, $b));
    }

}